<?php


namespace HabPan\Controllers\Users;

use HabPan\Controllers\Interfaces\BaseController;
use HabPan\Models\UserTypes\Hotel;

class ReinstallEmulator extends BaseController
{
    public function renderPage(): void
    {
        $tpl = $this->getTpl();
        $tpl->assign('PAGE_ID', 3);
        $tpl->assign('PAGE_TITLE', 'Reinstalar Emulador');

        $user = $this->getCore()->getSession()->getUser();
        $hotel = $user->getData();

        if (!($hotel instanceof Hotel)) {
            $this->getCore()->getRoute()->leave('/');
        }

        $tpl->assign('emulators', $this->getNodes()->getAvailableEmus($user->hasFuse('IsPremium')));

        $tpl->display('Reinstall/Emulator.tpl');
    }

    public function canEnter(): bool
    {
        return $this->getCore()->getSession()->isLogged();
    }
}