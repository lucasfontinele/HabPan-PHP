<?php


namespace HabPan\Controllers\Users;

use HabPan\Controllers\Interfaces\BaseController;
use HabPan\Models\UserTypes\Hotel;

class EditSwf extends BaseController
{
    public function renderPage(): void
    {
        $file = $this->getData('file');

        $tpl = $this->getTpl();

        $tpl->assign('PAGE_ID', 1);
        $tpl->assign('PAGE_TITLE', 'Editar Swf');

        $hotel = $this->getCore()->getSession()->getUser()->getData();
        if (!($hotel instanceof Hotel)) {
            exit;
        }

        if ($file === 'variables' || $file === 'texts') {
            $data = $hotel->getData($this->getCore()->getStorage(), $file);
            if ($data === '') {
                $data = file_get_contents(CWD . "repos/Swf/gamedata/$file.txt");
            }
            $tpl->assign('code', $data);
            $tpl->display('EditSwf.tpl');
            return;
        }
        $this->getCore()->getRoute()->leave('/');
    }

    public function canEnter(): bool
    {
        return $this->getCore()->getSession()->isLogged();
    }
}