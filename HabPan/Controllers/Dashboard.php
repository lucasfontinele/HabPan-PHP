<?php


namespace HabPan\Controllers;

use HabPan\Controllers\Interfaces\BaseController;

class Dashboard extends BaseController
{
    public function renderPage(): void
    {
        $tpl = $this->getTpl();

        $tpl->assign('PAGE_ID', 1);
        $tpl->assign('PAGE_TITLE', 'Dashboard');

        $tpl->display('Dashboard.tpl');
    }

    public function canEnter(): bool
    {
        return $this->getCore()->getSession()->isLogged();
    }
}