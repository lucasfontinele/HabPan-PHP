<?php


namespace HabPan\Controllers;

use HabPan\Controllers\Interfaces\BaseController;

class Index extends BaseController
{
    public function renderPage(): void
    {
        $tpl = $this->getTpl();

        $tpl->assign('PAGE_ID', 1);
        $tpl->assign('PAGE_TITLE', 'Fazer login');

//        var_dump($this->getCore()->getUsers()->encryptPass('y2W4J!pJVy16[j'));
        $tpl->display('Index.tpl');
    }

    public function canEnter(): bool
    {
        return !$this->getCore()->getSession()->isLogged();
    }
}