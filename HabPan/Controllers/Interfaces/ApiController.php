<?php

namespace HabPan\Controllers\Interfaces;

use HabPan\Core;
use HabPan\Services\Nodes;

abstract class ApiController implements IController
{
    private $data;
    private $core;
    private $node;

    public function __construct(Core $core, array $data)
    {
        $this->data = $data;
        $this->core = $core;

    }

    public function getNodes(): Nodes
    {
        if ($this->node === null) {
            $this->node = new Nodes($this->core->getStorage());
        }
        return $this->node;
    }

    public function renderPage(): void
    {
    }

    public function display($message): string
    {
        die(json_encode($message));
    }

    public function canEnter(): bool
    {
        if ($this->getCore()->getSession()->isLogged() && $this->getCore()->getSession()->getUser()->hasFuse('ApiLogin')) {
            return true;
        }

        $auth = $this->getData('auth');

        if (!is_array($auth)) {
            return false;
        }

        $user = $this->getCore()->getUsers()->tryLogin($auth[0], $auth[1]);

        if ($user === null) {
            return false;
        }

        if (!$user->hasFuse('ApiLogin')) {
            return false;
        }

        return true;
    }

    public function getCore(): Core
    {
        return $this->core;
    }

    public function getData(string $key)
    {
        return $this->data[$key] ?? null;
    }
}