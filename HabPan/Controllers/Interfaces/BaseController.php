<?php

namespace HabPan\Controllers\Interfaces;

use HabPan\Core;
use HabPan\Services\Nodes;
use Smarty;

abstract class BaseController implements IController
{
    private $data;
    private $tpl;
    private $core;
    private $node;

    public function __construct(Core $core, array $data)
    {
        $this->data = $data;
        $this->core = $core;
        $this->tpl = new Smarty();
        $this->tpl->setTemplateDir(VIEWS_DIR);
        $this->tpl->setCompileDir(CACHE_DIR);
        $this->tpl->assign('SITE_URL', SITE_URL);
        $this->tpl->assign('SITE_NAME', 'HabPan');
        $this->tpl->assign('CSRF_TOKEN', $core->getSession()->getData('CSRF_TOKEN'));
        $this->tpl->assign('session', $core->getSession());
    }


    public function getNodes(): Nodes
    {
        if ($this->node === null) {
            $this->node = new Nodes($this->core->getStorage());
        }
        return $this->node;
    }


    public function getData(string $key): ?string
    {
        return $this->data[$key] ?? null;
    }

    public function getCore(): Core
    {
        return $this->core;
    }

    public function getTpl(): Smarty
    {
        return $this->tpl;
    }

    public function renderPage(): void
    {

    }

    public function canEnter(): bool
    {
        return true;
    }
}