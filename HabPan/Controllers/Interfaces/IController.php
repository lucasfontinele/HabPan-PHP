<?php

namespace HabPan\Controllers\Interfaces;

interface IController
{

    public function getData(string $key);

    public function renderPage(): void;

    public function canEnter(): bool;
}