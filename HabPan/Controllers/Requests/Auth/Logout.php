<?php


namespace HabPan\Controllers\Requests\Auth;

use HabPan\Controllers\Interfaces\ApiController;

class Logout extends ApiController
{
    public function renderPage(): void
    {
        $this->getCore()->getSession()->destroy();
        $this->getCore()->getRoute()->leave('/');
    }

    public function canEnter(): bool
    {
        return $this->getCore()->getSession()->isLogged();
    }
}