<?php

namespace HabPan\Controllers\Requests\Auth;

use HabPan\Controllers\Interfaces\ApiController;

class Login extends ApiController
{
    public function renderPage(): void
    {
        $username = (string)$this->getData('username');
        $password = (string)$this->getData('password');

        $user = $this->getCore()->getUsers()->tryLogin($username, $password);
        if ($user === null) {
            $this->display(['message' => 'Usuário ou senha inválidos']);
        }

        $this->getCore()->getSession()->storeData('USER_ID', (string)$user->getId());
        $this->getCore()->getSession()->storeData('USER_HASH', $user->getPassword());
        $this->getCore()->getSession()->setUser($user);

        $this->display(['message' => 'success']);
    }

    public function canEnter(): bool
    {
        return !$this->getCore()->getSession()->isLogged();
    }
}