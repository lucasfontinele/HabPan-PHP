<?php


namespace HabPan\Controllers\Requests\Users;

use HabPan\Controllers\Interfaces\ApiController;
use HabPan\Models\UserTypes\Hotel;

class SwfFile extends ApiController
{
    public function renderPage(): void
    {
        $user_id = $this->getData('hotel');
        $file = $this->getData('file');
        if ($user_id === null || $file === null) {
            $this->display('not found');
        }
        $user = $this->getCore()->getUsers()->getById($user_id);
        if ($user === null) {
            $this->display('not found');
        }

        $hotel = $user->getData();
        if (!($hotel instanceof Hotel)) {
            $this->display('not an hotel');
        }

        if ($file !== 'variables' && $file !== 'texts') {
            $this->display('not found');
        }
        header('content-type: text/plain');
        ob_start();

        $data = $hotel->getData($this->getCore()->getStorage(), $file);

        if ($data === '') {
            $data = file_get_contents(CWD . "repos/Swf/gamedata/$file.txt");
        }

        echo $data;
        ob_flush();
        ob_end_flush();
    }

    public function canEnter(): bool
    {
        return true;
    }

}