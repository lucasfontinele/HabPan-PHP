<?php

namespace HabPan\Controllers\Requests\Users;

use HabPan\Controllers\Interfaces\ApiController;
use HabPan\Models\Remote\cPClient;
use HabPan\Models\UserTypes\Hotel;

class ConfigCms extends ApiController
{
    public function renderPage(): void
    {
        $session = $this->getCore()->getSession();
        $user = $session->getUser();

        if ($user === null) {
            $this->display('error');
        }

        $hotel = $user->getData();
        if (!($hotel instanceof Hotel)) {
            $this->display('not an hotel');
        }

        $cPanel = new cPClient($session);

        $pass = $session->getData('MYSQL_PASS');
        $node = null;
        if ($pass === null) {
            $pass = $session->randomHash(14);
            $this->getCore()->getSession()->storeData('MYSQL_PASS', $pass);
            $cPanel->recreateDatabaseUser($pass);
            $node = $this->getNodes()->getById($hotel->getServerId());
            if ($node === null) {
                $this->display('connection error');
            }
        }

        $cPanel->configCms( $pass, $node);

        $this->display('success');
    }

    public function canEnter(): bool
    {
        return $this->getCore()->getSession()->isLogged();
    }
}