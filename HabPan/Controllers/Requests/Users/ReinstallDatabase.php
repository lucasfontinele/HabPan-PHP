<?php

namespace HabPan\Controllers\Requests\Users;

use HabPan\Controllers\Interfaces\ApiController;
use HabPan\Models\Remote\cPClient;
use HabPan\Models\UserTypes\Hotel;
use mysqli;

class ReinstallDatabase extends ApiController
{
    public function renderPage(): void
    {
        $session = $this->getCore()->getSession();
        $user = $session->getUser();


        if ($user === null) {
            $this->display('error');
        }

        $hotel = $user->getData();
        if (!($hotel instanceof Hotel)) {
            $this->display('not an hotel');
        }

        $cPanel = new cPClient($session);
        $pass = $session->getData('MYSQL_PASS');
        $node = null;
        if ($pass === null) {
            $pass = $session->randomHash(14);
            $this->getCore()->getSession()->storeData('MYSQL_PASS', $pass);
            $cPanel->recreateDatabase($pass);
            $node = $this->getNodes()->getById($hotel->getServerId());
        }

        try {
            $db = new mysqli();
            $db->connect(CPANEL_SETTINGS['host'], $user->getUsername() . '_db', $pass, $user->getUsername() . '_db');
            $sql = file_get_contents(CWD . 'repos/Database/' . $hotel->getEmulator() . '.sql');
            $db->multi_query($sql);
        } catch (\Exception $e) {
            $this->display('mysql error');
        }

        if ($node !== null) {
            $cPanel->configCms(CWD . 'repos/Cms/settings.php', $pass, $node);
        }

        $this->display('success');

    }

    public function canEnter(): bool
    {
        return $this->getCore()->getSession()->isLogged();
    }
}