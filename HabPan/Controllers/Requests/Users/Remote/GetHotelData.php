<?php


namespace HabPan\Controllers\Requests\Users\Remote;

use HabPan\Controllers\Interfaces\ApiController;
use HabPan\Models\UserTypes\Hotel;

class GetHotelData extends ApiController
{
    public function renderPage(): void
    {
        $user_id = $this->getData('hotel');

        $user = $this->getCore()->getUsers()->getById($user_id);
        if ($user === null) {
            $this->display('not found');
        }

        $hotel = $user->getData();
        if (!($hotel instanceof Hotel)) {
            $this->display('not an hotel');
        }

        //todo: API call to the cms.
    }

    public function canEnter(): bool
    {
        return true;
    }

}