<?php

namespace HabPan\Controllers\Requests\Users;

use HabPan\Controllers\Interfaces\ApiController;
use HabPan\Models\Remote\cPClient;
use HabPan\Models\UserTypes\Hotel;

class ReinstallEmu extends ApiController
{
    public function renderPage(): void
    {
        $emu = (string)$this->getData('emu');
        $user = $this->getCore()->getSession()->getUser();

        if ($user === null) {
            $this->display(['message' => 'Erro!']);
        }

        $hotel = $user->getData();
        if (!($hotel instanceof Hotel)) {
            $this->display(['message' => 'Usuário inválido']);
        }

        if (!$this->getNodes()->validEmu($emu)) {
            $this->display(['message' => 'Emulador inválido!']);
        }

        $node = $this->getNodes()->getById($hotel->getServerId());
        if ($node === null) {
            $this->display(['message' => 'Erro ao conectar (1)']);
        }
        $node->setAuth($this->getCore()->getSession()->getData('SERVER_AUTH'));
        if (!$node->connect()) {
            $this->display(['message' => 'Erro ao conectar (2)']);
        }

        $cPanel = new cPClient($this->getCore()->getSession());

        $hotel->setEmulator($emu);

        $pass = $this->getCore()->getSession()->getData('MYSQL_PASS');
        if ($pass === null) {
            $pass = $this->getCore()->getSession()->randomHash(14);
            $this->getCore()->getSession()->storeData('MYSQL_PASS', $pass);
            $cPanel->recreateDatabaseUser($pass);
            $cPanel->configCms($pass, $node);
        } else {
            $node->sendMessage("reinstall $emu $pass");
        }
        $this->display(['message' => 'success']);
    }

    public function canEnter(): bool
    {
        return $this->getCore()->getSession()->isLogged();
    }
}