<?php

namespace HabPan\Controllers\Requests\Users;

use HabPan\Controllers\Interfaces\ApiController;
use HabPan\Models\Remote\cPClient;
use HabPan\Models\UserTypes\Hotel;

class ReinstallCms extends ApiController
{
    public function renderPage(): void
    {
        $session = $this->getCore()->getSession();
        $user = $session->getUser();
        $hotel = $user->getData();

        if (!($hotel instanceof Hotel)) {
            $this->display('Invalid user');
        }

        $cms = $this->getData('cms');

        $cms_path = CWD . 'repos/Cms/' . $cms . '.zip';
        if ($user === null) {
            $this->display('error');
        }

        if (!file_exists($cms_path)) {
            $this->display('Cms not found');
        }

        $hotel = $user->getData();
        if (!($hotel instanceof Hotel)) {
            $this->display('Not an hotel');
        }


        $cPanel = new cPClient($session);


        $ftp_pass = $session->getData('FTP_PASS');
        if ($ftp_pass === null) {
            $ftp_pass = $session->randomHash(14);
            $this->getCore()->getSession()->storeData('FTP_PASS', $ftp_pass);
            $cPanel->recreateFtp($ftp_pass);
        }

        $cPanel->deleteFile('public_html');


        $cPanel->execute_action(2, 'Fileman', 'mkdir', $user->getUsername(), [
            'path' => '/',
            'name' => 'public_html',
            'permissions' => '0777'
        ]);


        $accounts = json_decode($cPanel->execute_action(3, 'Ftp', 'list_ftp', $user->getUsername()), true);

        if (!isset($accounts['result']['data'])) {
            $this->display('error');
        }
        $ftp_user = null;
        foreach ($accounts['result']['data'] as $account) {
            if (!strpos($account['user'], 'panel@')) {
                continue;
            }
            $ftp_user = $account['user'];
            break;
        }


        $conn_id = ftp_connect(CPANEL_SETTINGS['host']);

        $login_result = ftp_login($conn_id, $ftp_user, $ftp_pass);

        if (!$login_result) {
            $this->display('ftp error');
        }

        ftp_put($conn_id, 'cms.zip', $cms_path, FTP_ASCII);

        ftp_close($conn_id);

        $cPanel->execute_action(2, 'Fileman', 'fileop', $user->getUsername(), [
            'op' => 'extract',
            'sourcefiles' => '/public_html/cms.zip',
            'destfiles' => '/public_html',
            'doubledecode' => 0,
            'metadata' => ''
        ]);

        $cPanel->deleteFile('/public_html/cms.zip');

        $node = null;

        $pass = $session->getData('MYSQL_PASS');
        if ($pass === null) {
            $pass = $session->randomHash(14);
            $this->getCore()->getSession()->storeData('MYSQL_PASS', $pass);
            $cPanel->recreateDatabaseUser($pass);
            $node = $this->getNodes()->getById($hotel->getServerId());
        }

        $cPanel->configCms(CWD . 'repos/Cms/settings.php', $pass, $node);

        $this->display('success');
    }

    public function canEnter(): bool
    {
        return $this->getCore()->getSession()->isLogged();
    }
}