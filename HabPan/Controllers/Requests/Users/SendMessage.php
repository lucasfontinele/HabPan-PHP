<?php

namespace HabPan\Controllers\Requests\Users;

use HabPan\Controllers\Interfaces\ApiController;
use HabPan\Models\UserTypes\Hotel;

class SendMessage extends ApiController
{
    public function renderPage(): void
    {
        //todo: reseller can send message

        $user_id = $this->getCore()->getSession()->getUser()->getId();
        $message = (string)$this->getData('cmd');

        $user = $this->getCore()->getUsers()->getById($user_id);

        if ($user === null) {
            $this->display(['message' => 'Erro!']);
        }

        $hotel = $user->getData();
        if (!($hotel instanceof Hotel)) {
            $this->display(['message' => 'Usuário inválido']);
        }

        $node = $this->getNodes()->getById($hotel->getServerId());
        if ($node === null) {
            $this->display(['message' => 'Erro ao conectar']);
        }

        $node->setAuth($this->getCore()->getSession()->getData('SERVER_AUTH'));
        if (!$node->connect()) {
            $this->display(['message' => 'Erro ao conectar']);
        }
        $node->sendMessage($message);

        $this->display(['message' => 'success']);
    }

    public function canEnter(): bool
    {
        return $this->getCore()->getSession()->isLogged();
    }
}