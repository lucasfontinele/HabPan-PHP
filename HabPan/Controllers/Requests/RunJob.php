<?php

namespace HabPan\Controllers;

use HabPan\Controllers\Interfaces\ApiController;

class RunJob extends ApiController
{
    public function renderPage(): void
    {
        $id = (int)$this->getData('id');
        $cron = $this->getCore()->getCron();
        $cron->closeConnection($id);
        $cron->executeJob($id);
    }

    public function canEnter(): bool
    {
        $whitelist = array(
            '127.0.0.1',
            '::1'
        );

        return in_array($_SERVER['REMOTE_ADDR'], $whitelist, false);
    }
}