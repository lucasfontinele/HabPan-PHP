<?php

namespace HabPan\Controllers\Requests\Management;

use HabPan\Controllers\Interfaces\ApiController;

class CheckUniqueEmail extends ApiController
{
    public function renderPage(): void
    {
        $data = $this->getData('data');
        if (!isset($data['email'])) {
            $this->display(false);
        }
        $mail = $data['email'];

        if ($this->getCore()->getUsers()->getByMail($mail) === null) {
            $this->display(true);
        }

        $this->display(false);
    }

}