<?php

namespace HabPan\Controllers\Requests\Management;

use HabPan\Controllers\Interfaces\ApiController;
use HabPan\Models\Remote\cPClient;

class GenUsername extends ApiController
{
    public function renderPage(): void
    {
        $cpanel = new cPClient(null);
        $name = 'hbr' . $this->uniqueId();

        $response = json_decode($cpanel->whmApi('verify_new_username', ['user' => $name]), true);

        if ($response['metadata']['result'] === 0) {
            var_dump($name);
            $this->display('error: already in use');
        }

        $this->display($name);
    }


    private function uniqueId($len = 11)
    {

        $hex = md5(uniqid('unique', true));

        $pack = pack('H*', $hex);
        $tmp = base64_encode($pack);

        $uid = preg_replace('#(*UTF8)[^a-z0-9]#', '', $tmp);

        $len = max(4, min(128, $len));

        while (\strlen($uid) < $len) {
            $uid .= $this->uniqueId(22);
        }

        return substr($uid, 0, $len);
    }


}