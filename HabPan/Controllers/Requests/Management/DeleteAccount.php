<?php

namespace HabPan\Controllers\Requests\Management;

use HabPan\Controllers\Interfaces\ApiController;
use HabPan\Models\Remote\cPClient;

class DeleteAccount extends ApiController
{
    public function renderPage(): void
    {
        $data = $this->getData('data');

        if (!isset($data['uid'])) {
            $this->display(false);
        }

        $id = $data['uid'];

        $user = $this->getCore()->getUsers()->getById($id);
        if ($user === null) {
            $this->display(false);
        }
        $cpanel = new cPClient(null);
        $cpanel->destroyAccount($user->getUsername());
        $user->destroy($this->getNodes());
        //todo
    }

}