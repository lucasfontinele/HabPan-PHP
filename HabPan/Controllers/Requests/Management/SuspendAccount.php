<?php

namespace HabPan\Controllers\Requests\Management;

use HabPan\Controllers\Interfaces\ApiController;
use HabPan\Models\Remote\cPClient;

class SuspendAccount extends ApiController
{
    public function renderPage(): void
    {
        $data = $this->getData('data');

        if (!isset($data['uid'])) {
            $this->display(false);
        }

        $id = (int)$data['uid'];

        $user = $this->getCore()->getUsers()->getById($id);
        if ($user === null) {
            $this->display(false);
        }

        $user->setStatus('disabled');
        $user->getData()->suspend($this->getNodes());
        $user->save(false);


        $cpanel = new cPClient(null);
        $cpanel->suspendAccount($user->getUsername(), $data['reason'] ?? 'No reason');
        $this->display(true);
    }

}