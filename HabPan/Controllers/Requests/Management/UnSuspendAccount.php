<?php

namespace HabPan\Controllers\Requests\Management;

use HabPan\Controllers\Interfaces\ApiController;
use HabPan\Models\Remote\cPClient;

class UnSuspendAccount extends ApiController
{
    public function renderPage(): void
    {
        $data = $this->getData('data');

        if (!isset($data['uid'])) {
            $this->display(false);
        }

        $id = (int)$data['uid'];

        $user = $this->getCore()->getUsers()->getById($id);
        if ($user === null) {
            $this->display(false);
        }

        $user->setStatus('enabled');
        $user->getData()->reactivate($this->getNodes());
        $user->save(false);


        $cpanel = new cPClient(null);
        $cpanel->reactivateAccount($user->getUsername());
        $this->display(true);
    }

}