<?php

namespace HabPan\Controllers\Requests\Management;

use HabPan\Controllers\Interfaces\ApiController;
use HabPan\Models\Remote\cPClient;

class CreateAccount extends ApiController
{
    public function renderPage(): void
    {

        $cpanel = new cPClient(null);
        $data = $this->getData('data');
        $name = $data['username'];
        $pass = $data['password'];
        $pkg = $data['package'];

        $data['real_name'] = ucfirst(explode('.', $data['hotel_domain'])[0]);

        $request = $cpanel->createAccount($data['hotel_domain'], $name, $pass, $pkg);
        $response = json_decode($request, true);

        if ($response['metadata']['result'] === 0) {
            $this->display(false);
        }

        $data['password'] = $this->getCore()->getUsers()->encryptPass($data['password']);
        $this->getCore()->getUsers()->newUser($data);

        $this->display(true);
    }

}