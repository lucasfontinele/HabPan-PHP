<?php

namespace HabPan\Controllers\Requests\Management;

use HabPan\Controllers\Interfaces\ApiController;

class GetUserId extends ApiController
{
    public function renderPage(): void
    {
        $data = $this->getData('data');

        if (!isset($data['username'])) {
            $this->display(false);
        }

        $name = $data['username'];

        $user = $this->getCore()->getUsers()->getByUsername($name);

        if ($user === null) {
            $this->display(false);
        }

        $this->display($user->getId());
    }

}