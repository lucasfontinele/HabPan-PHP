<?php

namespace HabPan\Services;

use HabPan\Models\Remote\Node;

class Nodes
{
    private $storage;

    public function __construct(Storage $storage)
    {
        $this->storage = $storage;
    }

    public function getAvailableEmus(bool $vip = false): array
    {
        if ($vip) {
            $data = $this->storage->query('SELECT * FROM emulators');
        } else {
            $data = $this->storage->query("SELECT * FROM emulators WHERE vip_only = '0'");
        }
        return $data->fetchAll(2);
    }

    public function validEmu(string $id): bool
    {
        $data = $this->storage->query('SELECT count(id) FROM emulators WHERE id = ?', $id)->fetchColumn();
        return $data > 0;
    }

    public function getById(int $id): ?Node
    {
        $data = $this->storage->query('SELECT hostname,port FROM servers WHERE id = ?', $id)->fetch(2);
        if (!$data) {
            return null;
        }

        return new Node($id, $data['hostname'], $data['port']);
    }

}