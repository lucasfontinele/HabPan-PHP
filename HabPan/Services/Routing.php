<?php

namespace HabPan\Services;

use FastRoute;
use FastRoute\RouteCollector;
use HabPan\Controllers\Dashboard;
use HabPan\Controllers\Index;
use HabPan\Controllers\Interfaces\IController;
use HabPan\Controllers\Requests\Auth\Login;
use HabPan\Controllers\Requests\Auth\Logout;
use HabPan\Controllers\Requests\Management\CheckUniqueEmail;
use HabPan\Controllers\Requests\Management\CreateAccount;
use HabPan\Controllers\Requests\Management\GenUsername;
use HabPan\Controllers\Requests\Management\GetUserId;
use HabPan\Controllers\Requests\Management\SuspendAccount;
use HabPan\Controllers\Requests\Management\UnSuspendAccount;
use HabPan\Controllers\Requests\Users\ConfigCms;
use HabPan\Controllers\Requests\Users\ReinstallCms;
use HabPan\Controllers\Requests\Users\ReinstallDatabase;
use HabPan\Controllers\Requests\Users\ReinstallEmu;
use HabPan\Controllers\Requests\Users\SendMessage;
use HabPan\Controllers\Users\EditSwf;
use HabPan\Controllers\Users\ReinstallEmulator;
use HabPan\Core;
use HabPan\Libs\Logging;

class Routing
{
    private $dispatcher;
    private $core;

    public function __construct(Core $core)
    {
        $this->core = $core;

        $this->dispatcher = FastRoute\cachedDispatcher(function (RouteCollector $r) {
            $r->addRoute('GET', '/[index]', Index::class);
            $r->addRoute('GET', '/dashboard', Dashboard::class);
            $r->addRoute('GET', '/editSwf/{file}', EditSwf::class);
            $r->addRoute('GET', '/reinstallEmu', ReinstallEmulator::class);
            $r->addRoute('GET', '/logout', Logout::class);

            $r->addGroup('/api', function (RouteCollector $r) {
                $r->addRoute('POST', '/sendLogin', Login::class);
                $r->addRoute(['GET', 'POST'], '/sendMessage[/{cmd}]', SendMessage::class);
                $r->addRoute(['GET', 'POST'], '/reinstallDatabase', ReinstallDatabase::class);
                $r->addRoute(['GET', 'POST'], '/reinstallCms[/{cms}]', ReinstallCms::class);
                $r->addRoute('POST', '/reinstallEmu', ReinstallEmu::class);
                $r->addRoute(['GET', 'POST'], '/configCms', ConfigCms::class);
                $r->addGroup('/manage', function (RouteCollector $r) {
                    $r->addRoute('POST', '/generateUsername', GenUsername::class);
                    $r->addRoute('POST', '/getUserId', GetUserId::class);
                    $r->addRoute('POST', '/uniqueMail', CheckUniqueEmail::class);
                    $r->addRoute('POST', '/createAccount', CreateAccount::class);
                    $r->addRoute('POST', '/suspendAccount', SuspendAccount::class);
                    $r->addRoute('POST', '/unsuspendAccount', UnSuspendAccount::class);
                });
            });
        }, [
            'cacheFile' => INCLUDES . '/Cache/route.cache', /* required */
            'cacheDisabled' => DEBUG,
        ]);
    }


    public function renderPage(): void
    {
        $http_method = $_SERVER['REQUEST_METHOD'];
        $request_uri = $_SERVER['REQUEST_URI'];

        if (DEBUG) {
            $data = $_SERVER['REQUEST_URI'] . ' -- '
                . json_encode(array_merge($_GET, $_POST)) . "  \n\r";
            Logging::LogToFile(Logging::levels['Debug'], $data);
        }

        if (false !== $position = strpos($request_uri, '?')) {
            $request_uri = substr($request_uri, 0, $position);
        }
        $request_uri = rawurldecode($request_uri);

        $route_args = $this->dispatcher->dispatch($http_method, $request_uri);
        switch ($route_args[0]) {
            case FastRoute\Dispatcher::NOT_FOUND:
                http_response_code(404);
                break;
            case FastRoute\Dispatcher::METHOD_NOT_ALLOWED:
                http_response_code(405);
                break;
            case FastRoute\Dispatcher::FOUND:
                $handler = $route_args[1];

                $data = array_merge($_POST, $route_args[2]);
                /** @var IController $request */
                $request = new $handler($this->core, $data);

                try {
                    if ($request->canEnter()) {
                        $request->renderPage();
                    } else {
                        if ($this->core->getSession()->isLogged()) {
                            $this->leave('/dashboard');
                        } else {
                            $this->leave('/');
                        }

                        exit;
                    }
                } catch (\Exception $ex) {
                    Logging::HandleException($ex, true);
                }

                break;
        }
    }

    public function leave($url): void
    {
        header('Location: ' . $url);
        exit;
    }

}