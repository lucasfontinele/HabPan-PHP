<?php

namespace HabPan\Services;

use Ds\Map;
use PDO;
use Redis;

class Storage
{
    private $connection;

    private $cacheConn;

    private $container;


    public function __construct(array $config, bool $cache = true)
    {
        try {
            $this->connection = new PDO('mysql:host=' . $config['host'] . ';dbname=' . $config['db'] . ';charset=utf8', $config['user'], $config['pass']);
            $this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (\Exception $ex) {
            echo $ex;
            die('MYSQL ERROR!');
        }

        if (!$cache) {
            return;
        }

        try {
            $this->container = new Map();
            $this->cacheConn = new Redis();
            $this->cacheConn->pconnect(CACHE_SETTINGS['host'], CACHE_SETTINGS['port'], 0);
            $this->cacheConn->auth(CACHE_SETTINGS['pass']);
        } catch (\Exception $ex) {
            die('Cache Error!');
        }
    }

    public function lastInsertId(): int
    {
        return (int)$this->connection->lastInsertId();
    }

    public function limitedQuery(string $sql, ...$params): \PDOStatement
    {
        $this->connection->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
        $prep = $this->connection->prepare($sql);
        $prep->execute($params);
        return $prep;
    }

    public function normalQuery(string $query): \PDOStatement
    {
        return $this->connection->query($query);
    }

    public function fastQuery(string $sql): void
    {
        $this->connection->exec($sql);
    }

    public function fromCache(string $key)
    {
        $data = $this->container->get($key, null);

        if ($data === null && $this->cacheConn->exists($key)) {
            $data = $this->cacheConn->get($key);
            return igbinary_unserialize($data);
        }

        return $data;
    }


    public function toCache(string $key, $entry, int $expire = 3600): bool
    {
        $this->container->put($key, $entry);
        return $this->cacheConn->set($key, igbinary_serialize($entry), $expire);
    }

    public function query(string $sql, ...$params): \PDOStatement
    {
        $prep = $this->connection->prepare($sql);
        $prep->execute($params);
        return $prep;
    }

}
