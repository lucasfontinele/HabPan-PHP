<?php

namespace HabPan\Services;

use Ds\Map;

class Ranks
{
    /**
     * @var Map
     */
    private static $ranks;

    public static function initialize(Storage $storage): void
    {
        self::$ranks = new Map();
        $data = $storage->fromCache('ranks');
        if (!$data) {
            $data = $storage->normalQuery('select * from ranks')->fetchAll(2);
            foreach ($data as $row) {
                self::$ranks->put((int)$row['id'], $row);
            }

            $storage->toCache('ranks', self::$ranks);

            return;
        }
        self::$ranks = $data;
    }

    public static function hasFuse(int $rank, string $fuse): bool
    {
        $rank_data = self::$ranks->get($rank, []);

        if (!isset($rank_data[$fuse])) {
            return false;
        }

        return $rank_data[$fuse] === '1';
    }
}