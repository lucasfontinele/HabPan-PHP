<?php

namespace HabPan\Services;

use HabPan\Models\User;
use HabPan\Models\UserTypes\Hotel;
use HabPan\Models\UserTypes\Reseller;

class Users
{
    private $storage;

    public function __construct(Storage $storage)
    {
        $this->storage = $storage;
    }

    public function tryLogin(string $name, string $pass): ?User
    {
        $user = $this->getByUsername($name, true);
        if ($user === null) {
            return null;
        }
        if (!$this->checkPass($pass, $user->getPassword())) {
            return null;
        }
        if ($user->getStatus() === 'disabled') {
            return null;
        }

        return $user;
    }

    public function getByUsername(string $name, bool $ignoreCache = false): ?User
    {
        $user = $ignoreCache ? null : $this->storage->fromCache("user-$name");
        if ($user === null) {
            $data = $this->storage->query('SELECT * FROM users WHERE username = ?', $name)->fetch(2);
            if ($data) {
                $user = $this->getUser($data);
            }
        }
        return $user;
    }

    private function getUser(array $data): User
    {
        $user = new User($data['id'], $this->storage);
        $user->fetchData($data);
        $user->initUserData();
        return $user;
    }

    public function checkPass(string $text, string $hash): bool
    {
        return password_verify($text . PASS_BLOW, $hash);
    }

    public function encryptPass(string $text): string
    {
        return password_hash($text . PASS_BLOW, PASSWORD_DEFAULT);
    }

    public function newUser(array $data): void
    {
        $user = new User(null, null);
        $user->fetchData($data);
        $user->save(true);
        if (Ranks::hasFuse($data['rank'], 'IsReseller')) {
            $type = new Reseller($user->getId(), $this->storage);
        } else {
            $type = new Hotel($user->getId(), $this->storage);
        }
        $type->fetchData($data);
        $type->save(true);
    }

    public function getById(int $id, bool $ignoreCache = false): ?User
    {
        $user = $ignoreCache ? null : $this->storage->fromCache("user-$id");
        if ($user === null) {
            $data = $this->storage->query('SELECT * FROM users WHERE id = ?', $id)->fetch(2);
            if ($data) {
                $user = $this->getUser($data);
            }
        }
        return $user;
    }

    public function getByMail(string $mail, bool $ignoreCache = false): ?User
    {
        $user = $ignoreCache ? null : $this->storage->fromCache("user-$mail");
        if ($user === null) {
            $data = $this->storage->query('SELECT * FROM users WHERE mail = ?', $mail)->fetch(2);
            if ($data) {
                $user = $this->getUser($data);
            }
        }
        return $user;
    }
}