<?php

namespace HabPan\Services;

class Cron
{
    private $storage;

    public function __construct(Storage $storage)
    {
        $this->storage = $storage;
    }

    public function closeConnection(int $job): void
    {
        ignore_user_abort(true);
        set_time_limit(0);
        ob_end_clean();
        header('Connection: close');
        ignore_user_abort(true);
        ob_start();

        echo('Started cron ' . $job);

        $size = ob_get_length();
        header("Content-Length: $size");
        header('Content-Encoding: none');
        ob_end_flush();
        flush();
    }

    public function runCron(string $siteUrl): void
    {
        $query = $this->storage->normalQuery("SELECT id,last_exec,exec_every FROM crons WHERE enabled = '1' ORDER BY prio ASC");
        while ($cron_row = $query->fetch(2)) {
            if ($this->getNextExec($cron_row) <= time()) {
                $this->asyncCurl("$siteUrl/runJob/{$cron_row['id']}");
            }
        }
    }

    private function getNextExec(array $data): int
    {
        if (empty($data)) {
            return -1;
        }

        return $data['last_exec'] + $data['exec_every'];
    }

    public function asyncCurl(string $url)
    {
        $c = curl_init();
        curl_setopt($c, CURLOPT_URL, $url);
        curl_setopt($c, CURLOPT_FOLLOWLOCATION, true);  // Follow the redirects (needed for mod_rewrite)
        curl_setopt($c, CURLOPT_HEADER, false);         // Don't retrieve headers
        curl_setopt($c, CURLOPT_NOBODY, true);          // Don't retrieve the body
        curl_setopt($c, CURLOPT_RETURNTRANSFER, true);  // Return from curl_exec rather than echoing
        curl_setopt($c, CURLOPT_FRESH_CONNECT, true);   // Always ensure the connection is fresh
        curl_setopt($c, CURLOPT_NOSIGNAL, 1);
        curl_setopt($c, CURLOPT_TIMEOUT, 1);

        return curl_exec($c);
    }

    public function executeJob(int $jobId): void
    {
        $file_name = $this->storage->query('SELECT scriptfile FROM crons WHERE id = ?', $jobId)->fetchColumn();

        $path = INCLUDES . 'CronJobs/' . $file_name;
        if (!file_exists($path)) {
            echo "Could'nt execute cron $file_name";
            return;
        }

        include $path;

        $this->storage->query('UPDATE crons SET last_exec = ? WHERE id = ? LIMIT 1', $jobId, time());
    }
}