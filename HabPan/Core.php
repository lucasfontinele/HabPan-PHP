<?php

namespace HabPan;

use HabPan\Services\Cron;
use HabPan\Services\Ranks;
use HabPan\Services\Routing;
use HabPan\Services\Storage;
use HabPan\Services\Users;
use HabPan\Models\Session;

class Core
{
    private $storage;
    private $route;
    private $session;

    /**
     * @var Users
     */
    private $users;

    public function __construct()
    {
        $this->storage = new Storage(MYSQL_SETTINGS);
        $this->session = new Session();

        if ($this->session->checkUser($this) && $this->session->getData('SERVER_AUTH') === null) {
            $this->session->storeData('SERVER_AUTH', $this->session->randomHash(64));
        }

        if ($this->session->getData('CSRF_TOKEN') === null) {
            $this->session->storeData('CSRF_TOKEN', $this->session->randomHash(64));
        }

        Ranks::initialize($this->storage);
        $this->route = new Routing($this);
    }

    public function getUsers(): Users
    {
        return $this->users ?? $this->users = new Users($this->storage);
    }

    public function getRoute(): Routing
    {
        return $this->route;
    }

    public function getSession(): Session
    {
        return $this->session;
    }

    public function getCron(): Cron
    {
        return new Cron($this->getStorage());
    }

    public function getStorage(): Storage
    {
        return $this->storage;
    }
}