<?php

namespace HabPan\Models\Remote;

use GuzzleHttp\Client;
use HabPan\Models\Session;
use HabPan\Models\UserTypes\Hotel;

class cPClient
{
    public $targetName;
    /**
     * @var string[] Sets of headers that will be sent at request.
     *
     * @since v1.0.0
     */
    protected $headers = array();
    /**
     * @var integer Query timeout (Guzzle option)
     *
     * @since v1.0.0
     */
    protected $timeout = 10;
    /**
     * @var integer Connection timeout (Guzzle option)
     *
     * @since v1.0.0
     */
    protected $connection_timeout = 10;
    private $session;
    /**
     * @var string Username of your whm server. Must be string
     *
     * @since v1.0.0
     */
    private $username;
    /**
     * @var string Password or long hash of your whm server.
     *
     * @since v1.0.0
     */
    private $password;
    /**
     * @var string Authentication type you want to use. You can set as 'hash' or 'password'.
     *
     * @since v1.0.0
     */
    private $auth_type;
    /**
     * @var string Host of your whm server. You must set it with full host with its port and protocol.
     *
     * @since v1.0.0
     */
    private $host;

    public function __construct(?Session $session = null)
    {
        $options = [
            'host' => 'https://' . CPANEL_SETTINGS['host'] . ':' . CPANEL_SETTINGS['port'],
            'username' => CPANEL_SETTINGS['user'],
            'auth_type' => 'hash',
            'password' => CPANEL_SETTINGS['token'],
        ];

        if (!empty($options['auth_type'])) {
            $this->setAuthType($options['auth_type']);
        }

        $this->checkOptions($options)
            ->setHost($options['host'])
            ->setAuthorization($options['username'], $options['password']);

        if ($session !== null) {
            $this->session = $session;
            $this->targetName = $this->session->getUser()->getUsername();
        }
    }

    /**
     * set Authentication Type.
     *
     * @param string $auth_type Authentication type for calling API.
     *
     * @return object return as self-object
     *
     * @since v1.0.0
     */
    public function setAuthType($auth_type)
    {
        $this->auth_type = $auth_type;

        return $this;
    }

    /**
     * checking options for 'username', 'password', and 'host'. If they are not set, some exception will be thrown.
     *
     * @param array $options list of options that will be checked
     *
     * @return self
     * @throws \Exception
     * @since v1.0.0
     */
    private function checkOptions($options)
    {
        if (empty($options['username'])) {
            throw new \RuntimeException('Username is not set', 2301);
        }
        if (empty($options['password'])) {
            throw new \RuntimeException('Password or hash is not set', 2302);
        }
        if (empty($options['host'])) {
            throw new \RuntimeException('CPanel Host is not set', 2303);
        }

        return $this;
    }

    /**
     * Magic method who will call the CPanel/WHM Api.
     *
     * @param string $function function name that will be called
     * @param array $arguments parameter that should be passed when calling API function
     *
     * @return string (json-array) result of called functions
     *
     * @since v1.0.0
     */
    public function __call($function, $arguments)
    {
        if (count($arguments) > 0)
            $arguments = $arguments[0];
        return $this->runQuery($function, $arguments);
    }

    /**
     * The executor. It will run API function and get the data.
     *
     * @param string $action function name that will be called.
     * @param string[] $arguments list of parameters that will be attached.
     *
     * @return string results of API call
     *
     * @since v1.0.0
     */
    protected function runQuery(string $action, array $arguments): ?string
    {
        $host = $this->getHost();
        $client = new Client(['base_uri' => $host]);
        try {
            $response = $client->post('/json-api/' . $action, [
                'headers' => $this->createHeader(),
                // 'body'    => $arguments[0],
                'verify' => false,
                'query' => $arguments,
                'timeout' => $this->getTimeout(),
                'connect_timeout' => $this->getConnectionTimeout()
            ]);

            return (string)$response->getBody();
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            return $e->getMessage();
        }
    }

    /**
     * get host of your whm server.
     *
     * @return string host of your whm server
     *
     * @since v1.0.0
     */
    public function getHost()
    {
        return $this->host;
    }

    /**
     * set API Host.
     *
     * @param string $host Host of your whm server.
     *
     * @return object return as self-object
     *
     * @since v1.0.0
     */
    public function setHost($host)
    {
        $this->host = $host;

        return $this;
    }

    /**
     * Extend HTTP headers that will be sent.
     *
     * @return array of headers that will be sent
     *
     * @since v1.0.0
     */
    private function createHeader()
    {
        $headers = $this->headers;

        $username = $this->username;
        $auth_type = $this->auth_type;

        if ($auth_type === 'hash') {
            $headers['Authorization'] = 'WHM ' . $username . ':' . preg_replace("'(\r|\n|\s|\t)'", '', $this->getPassword());
        } elseif ($auth_type === 'password') {
            $headers['Authorization'] = 'Basic ' . base64_encode($username . ':' . $this->getPassword());
        }
        return $headers;
    }

    /**
     * get password or long hash.
     *
     * @return string get password or long hash
     *
     * @since v1.0.0
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * get timeout.
     *
     * @return integer timeout of the Guzzle request
     *
     * @since v1.0.0
     */
    public function getTimeout()
    {
        return $this->timeout;
    }

    /**
     * set timeout.
     *
     * @param $timeout
     *
     * @return object return as self-object
     *
     * @since v1.0.0
     */
    public function setTimeout($timeout)
    {
        $this->timeout = (int)$timeout;

        return $this;
    }

    /**
     * get connection timeout.
     *
     * @return integer connection timeout of the Guzzle request
     *
     * @since v1.0.0
     */
    public function getConnectionTimeout(): int
    {
        return $this->connection_timeout;
    }

    /**
     * set connection timeout.
     *
     * @param $connection_timeout
     *
     * @return object return as self-object
     *
     * @since v1.0.0
     */
    public function setConnectionTimeout($connection_timeout)
    {
        $this->connection_timeout = (int)$connection_timeout;

        return $this;
    }

    public function whmApi(string $action, array $params = null): ?string
    {
        $params['api.version'] = 1;
        return $this->runQuery($action, $params);
    }

    public function deleteFile(string $file): void
    {
        $this->execute_action(2, 'Fileman', 'fileop', $this->targetName, [
            'op' => 'unlink',
            'sourcefiles' => $file,
            'doubledecode' => '0',
            'metadata' => ''
        ]);
    }

    /**
     * Use cPanel API 1 or use cPanel API 2 or use UAPI.
     *
     * @param $api (1 = cPanel API 1, 2 = cPanel API 2, 3 = UAPI)
     * @param $module
     * @param $function
     * @param $username
     * @param array $params
     * @return mixed
     * @throws \Exception
     */
    public function execute_action(int $api, string $module, string $function, string $username, array $params = [])
    {
        $action = 'cpanel';
        $params = array_merge($params, [
            'cpanel_jsonapi_apiversion' => $api,
            'cpanel_jsonapi_module' => $module,
            'cpanel_jsonapi_func' => $function,
            'cpanel_jsonapi_user' => $username,
        ]);
        return $this->runQuery($action, $params);
    }

    public function recreateDatabase(string $pass): void
    {
        $this->execute_action(3, 'Mysql', 'delete_database', $this->targetName, ['name' => $this->targetName . '_db']);
        $this->execute_action(3, 'Mysql', 'create_database', $this->targetName, ['name' => $this->targetName . '_db']);

        $this->recreateDatabaseUser($pass);
    }

    public function recreateDatabaseUser(string $pass): void
    {
        $this->execute_action(3, 'Mysql', 'delete_user', $this->targetName, ['name' => $this->targetName . '_db']);
        $this->execute_action(3, 'Mysql', 'create_user', $this->targetName, [
            'name' => $this->targetName . '_db',
            'password' => $pass
        ]);

        $this->execute_action(3, 'Mysql', 'set_privileges_on_database', $this->targetName, [
            'user' => $this->targetName . '_db',
            'database' => $this->targetName . '_db',
            'privileges' => 'ALL PRIVILEGES'
        ]);
    }

    public function recreateFtp(string $pass): void
    {
        $this->execute_action(3, 'Ftp', 'delete_ftp', $this->targetName,
            [
                'user' => 'panel',
                'destroy' => '1',
            ]
        );
        $this->execute_action(3, 'Ftp', 'add_ftp', $this->targetName, [
                'user' => 'panel',
                'pass' => $pass,
                'quota' => '0',
                'homedir' => 'public_html'
            ]
        );
    }

    public function configCms(string $mysql_pass, Node $node = null): string
    {
        $path = CWD . 'repos/Cms/settings.php';

        $hotel = $this->session->getUser()->getData();

        if (!($hotel instanceof Hotel)) {
            return 'not an hotel';
        }

        $file = file_get_contents($path);
        $file = sprintf($file, CPANEL_SETTINGS['host'], $mysql_pass, $this->targetName . '_db', $this->targetName . '_db');
        $this->execute_action(3, 'Fileman', 'save_file_content', $this->targetName, [
            'dir' => '/public_html',
            'file' => 'config.php',
            'content' => $file
        ]);


        if ($node === null) {
            return 'success';
        }

        $node->setAuth($this->session->getData('SERVER_AUTH'));
        $node->connect();
        $node->sendMessage('reinstall ' . $hotel->getEmulator() . ' ' . $mysql_pass);

        return 'success';
    }

    public function createAccount($domain_name, $username, $password, $plan): ?string
    {
        return $this->runQuery('createacct', [
            'username' => $username,
            'domain' => $domain_name,
            'password' => $password,
            'plan' => $plan,
        ]);
    }

    public function destroyAccount($username): ?string
    {
        return $this->runQuery('removeacct', [
            'username' => $username,
        ]);
    }

    public function suspendAccount($username, string $reason): ?string
    {
        return $this->runQuery('suspendacct', [
            'user' => $username,
            'reason' => $reason
        ]);
    }

    public function reactivateAccount($username): ?string
    {
        return $this->runQuery('unsuspendacct', [
            'user' => $username
        ]);
    }

    /**
     * set authorization for access.
     * It only set 'username' and 'password'.
     *
     * @param string $username Username of your whm server.
     * @param string $password Password or long hash of your whm server.
     *
     * @return self
     *
     * @since v1.0.0
     */
    public function setAuthorization($username, $password): self
    {
        $this->username = $username;
        $this->password = $password;

        return $this;
    }

    public function setHeader($name, $value = '')
    {
        $this->headers[$name] = $value;

        return $this;
    }

    public function cpanel($module, $function, $username, $params = array()): ?string
    {
        $action = 'cpanel';
        $params = array_merge($params, [
            'cpanel_jsonapi_version' => 2,
            'cpanel_jsonapi_module' => $module,
            'cpanel_jsonapi_func' => $function,
            'cpanel_jsonapi_user' => $username,
        ]);

        return $this->runQuery($action, $params);
    }
}
