<?php

namespace HabPan\Models\Remote;

class Node
{
    private $id;
    private $host;
    private $port;

    private $socket;
    private $connected;


    private $auth;

    public function __construct(int $id, string $host, int $port)
    {
        $this->id = $id;
        $this->host = $host;
        $this->port = $port;
    }

    public function setAuth(string $auth): void
    {
        $this->auth = $auth;
    }

    public function sendMessage(string ...$data): void
    {
        if (!$this->connected) {
            $this->connect();
        }
        $toSend = implode(' ', $data);

        socket_write($this->socket, $toSend, strlen($toSend));
        flush();
        usleep(1000);
    }

    public function connect(): bool
    {
        $this->socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);

        socket_set_nonblock($this->socket);
        $this->connected = true;
        if (!socket_connect($this->socket, $this->host, $this->port)) {
//            return false;
        }

        $this->sendMessage('auth', $this->auth);

        usleep(1000);
        return true;
    }

    public function disconnect(): void
    {
        socket_close($this->socket);
    }
}