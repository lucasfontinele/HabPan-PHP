<?php

namespace HabPan\Models\UserTypes;

use HabPan\Services\Nodes;
use HabPan\Services\Storage;
use HabPan\Models\Interfaces\IUserType;

class Reseller implements IUserType
{
    private $user_id;
    private $owner_id;
    private $server_id;

    private $credits;
    private $pixels;

    private $storage;

    public function __construct(int $user, Storage $storage)
    {
        $this->storage = $storage;
    }

    public function suspend(Nodes $nodes)
    {
        $data = $this->storage->query('SELECT user_id,server_id FROM hotels WHERE owner_id = ?', $this->user_id)->fetchAll(2);
        foreach ($data as $row) {
            $node = $nodes->getById($row['server_id']);
            if ($node === null) {
                continue;
            }
            $node->sendMessage('suspend', $row['user_id']);
        }
    }
    public function reactivate(Nodes $nodes)
    {
        $data = $this->storage->query('SELECT user_id,server_id FROM hotels WHERE owner_id = ?', $this->user_id)->fetchAll(2);
        foreach ($data as $row) {
            $node = $nodes->getById($row['server_id']);
            if ($node === null) {
                continue;
            }
            $node->sendMessage('reactivate', $row['user_id']);
        }

    }

    public function destroy(Nodes $nodes)
    {

    }

    public function getUserId(): int
    {
        // TODO: Implement getUserId() method.
    }

    public function getRank(): int
    {
        // TODO: Implement getRank() method.
    }

    public function getData(string $key): string
    {
        // TODO: Implement getData() method.
    }

    public function fetchData(array $data): void
    {
        // TODO: Implement fetchData() method.
    }

    public function save(bool $insert): void
    {
        // TODO: Implement save() method.
    }
}