<?php

namespace HabPan\Models\UserTypes;

use HabPan\Services\Nodes;
use HabPan\Services\Storage;
use HabPan\Models\Interfaces\IUserType;

class Hotel implements IUserType
{
    private $user_id;
    private $server_id;
    private $cms_id;
    private $emulator_id;

    private $tcp_port;
    private $mus_port;
    private $hotel_domain;

    private $storage;

    public function __construct(int $user, Storage $storage)
    {
        $this->user_id = $user;
        $this->storage = $storage;
    }

    public function getServerId(): int
    {
        return $this->server_id;
    }

    public function getEmulator(): string
    {
        return $this->emulator_id;
    }

    public function setEmulator(string $emu): void
    {
        $this->emulator_id = $emu;
    }

    public function getCms(): string
    {
        return $this->cms_id;
    }


    public function suspend(Nodes $nodes)
    {
        $node = $nodes->getById($this->server_id);
        if ($node === null) {
            return;
        }
        $node->sendMessage('suspend', $this->user_id);
    }

    public function reactivate(Nodes $nodes)
    {
        $node = $nodes->getById($this->server_id);
        if ($node === null) {
            return;
        }
        $node->sendMessage('reactivate', $this->user_id);
    }

    public function destroy(Nodes $nodes)
    {

    }

    public function generateAuth(Storage $db): void
    {
        $db->query('REPLACE INTO servers_auth (user_id,server_id, auth_key, expiration) VALUES (?,?,?,?)', $this->user_id, $this->server_id, $_SESSION['SERVER_AUTH'], time() + 3600);
    }

    public function getData(Storage $db, string $key): string
    {
        $data = $db->fromCache($key);
        if ($data === null) {
            $data = $db->query("SELECT $key FROM hotels_data WHERE user_id = ?", $this->user_id)->fetchColumn();
            if ($data !== null) {
                $db->toCache($key, $data);
            }
        }
        return $data;
    }

    public function getUserId(): int
    {
        return $this->user_id;
    }


    public function fetchData(array $data): void
    {
        $this->server_id = $data['server_id'];
        $this->tcp_port = $data['tcp_port'];
        $this->mus_port = $data['mus_port'];
        $this->emulator_id = $data['emulator_id'];
        $this->cms_id = $data['cms_id'];
        $this->hotel_domain = $data['hotel_domain'];
    }

    public function save(bool $insert): void
    {
        if ($insert) {
            $this->storage->query('INSERT INTO hotels (user_id,hotel_domain) VALUE (?,?)', $this->user_id, $this->hotel_domain);
            $this->storage->query('INSERT INTO hotels_data (user_id) VALUE (?)', $this->user_id);
        }
    }
}