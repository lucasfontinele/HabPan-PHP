<?php

Namespace HabPan\Models\Interfaces;

interface IQueryable
{
    public function fetchData(array $data): void;

    public function save(bool $insert): void;
}