<?php

namespace HabPan\Models\Interfaces;

use HabPan\Services\Nodes;

interface IUserType extends IQueryable
{
    public function getUserId(): int;

    public function suspend(Nodes $nodes);

    public function reactivate(Nodes $nodes);

    public function destroy(Nodes $nodes);

}