<?php

namespace HabPan\Models;

use HabPan\Core;

class Session
{


    /**
     * @var User
     */
    private $user;

    public function isLogged(): bool
    {
        return $this->user !== null;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(User $user): void
    {
        $this->user = $user;
    }

    public function checkUser(Core $core): bool
    {
        $userId = (int)($this->getData('USER_ID') ?? 0);
        if ($userId > 0) {
            $user = $core->getUsers()->getById($userId);
            if ($user === null) {
                $this->destroy();
                return false;
            }

            $pass = $this->getData('USER_HASH');
            if (($pass !== null) && $pass === $user->getPassword()) {
                $this->user = $user;
                return true;
            }

            $this->destroy();
        }

        return false;
    }


    public function randomHash(int $len = 100): string
    {
        try {
            $data = base64_encode(random_bytes($len));
        } catch (\Exception $e) {
            $data = $this->encrypt(uniqid('token', true));
        }
        return substr($data, 0, $len);
    }

    public function encrypt($text = 'f'): string
    {
        return password_hash($text, PASSWORD_DEFAULT);
    }


    public function getData(string $key): ?string
    {
        return $_SESSION[$key] ?? null;
    }

    public function destroy(): void
    {
        $params = session_get_cookie_params();
        setcookie(session_name(), '', time() - 42000,
            $params['path'], $params['domain'],
            $params['secure'], $params['httponly']
        );

        session_unset();
        session_destroy();
        $this->user = null;
    }


    public function storeData(string $key, string $value): void
    {
        $_SESSION[$key] = $value;
    }

}