<?php

namespace HabPan\Libs;

use Exception;

class Logging
{

    public const levels = [
        'Info' => 'infos.txt',
        'Error' => 'errors.txt',
        'Critical' => 'critical.txt',
        'Debug' => 'debug.txt'
    ];

    public static function HandleException(Exception $ex, bool $display = false): void
    {
        if ($display) {
            self::Display($ex);
        }
        self::LogToFile(self::levels['Error'], $ex);
    }

    public static function Display(string $message): void
    {
        $message = str_replace(["\r", ' '], ['</p><p>', '&nbsp;'], $message);
        echo $message;
    }

    public static function LogToFile(string $file, string $text): void
    {
        file_put_contents(LOGS_DIR . $file, $text, FILE_APPEND);
    }
}