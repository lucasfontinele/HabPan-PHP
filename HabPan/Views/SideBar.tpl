<!-- Main sidebar -->
<div class="sidebar sidebar-dark sidebar-main sidebar-expand-md">

    <!-- Sidebar mobile toggler -->
    <div class="sidebar-mobile-toggler text-center">
        <a href="#" class="sidebar-mobile-main-toggle">
            <i class="icon-arrow-left8"></i>
        </a>
        Navigation
        <a href="#" class="sidebar-mobile-expand">
            <i class="icon-screen-full"></i>
            <i class="icon-screen-normal"></i>
        </a>
    </div>
    <!-- /sidebar mobile toggler -->


    <!-- Sidebar content -->
    <div class="sidebar-content">

        <!-- User menu -->
        <div class="sidebar-user">
            <div class="card-body">
                <div class="media">
                    <div class="mr-3">
                        <a href="#"><img src="{$SITE_URL}/assets/images/placeholders/placeholder.jpg"
                                         width="38" height="38" class="rounded-circle" alt=""></a>
                    </div>

                    <div class="media-body">
                        <div class="media-title font-weight-semibold">{$session->getUser()->getName()}</div>
                        <div class="font-size-xs opacity-50">
                            <i class="icon-pin font-size-sm"></i> &nbsp;Santa Ana, CA
                        </div>
                    </div>

                    <div class="ml-3 align-self-center">
                        <a href="#" class="text-white"><i class="icon-cog3"></i></a>
                    </div>
                </div>
            </div>
        </div>
        <!-- /user menu -->


        <!-- Main navigation -->
        <div class="card card-sidebar-mobile">
            <ul class="nav nav-sidebar" data-nav-type="accordion">

                <!-- Main -->
                <li class="nav-item-header">
                    <div class="text-uppercase font-size-xs line-height-xs">Principal</div>
                    <i class="icon-menu" title="Main"></i></li>
                <li class="nav-item">
                    <a href="{$SITE_URL}" class="nav-link active">
                        <i class="icon-home4"></i>
                        <span>
									Dashboard
								</span>
                    </a>
                </li>

                <li class="nav-item nav-item-submenu">
                    <a href="#" class="nav-link"><i class="icon-user"></i> <span>Sua conta</span></a>

                    <ul class="nav nav-group-sub" data-submenu-title="Layouts">
                        <li class="nav-item"><a href="../../../../layout_1/LTR/default/full/{$SITE_URL}"
                                                class="nav-link">Default layout</a></li>
                        <li class="nav-item"><a href="{$SITE_URL}" class="nav-link active">Layout 2</a></li>
                        <li class="nav-item"><a href="../../../../layout_3/LTR/default/full/{$SITE_URL}"
                                                class="nav-link">Layout 3</a></li>
                        <li class="nav-item"><a href="../../../../layout_4/LTR/default/full/{$SITE_URL}"
                                                class="nav-link">Layout 4</a></li>
                        <li class="nav-item"><a href="../../../../layout_5/LTR/default/full/{$SITE_URL}"
                                                class="nav-link">Layout 5</a></li>
                        <li class="nav-item"><a href="../../../../layout_6/LTR/default/full/{$SITE_URL}"
                                                class="nav-link disabled">Layout 6 <span
                                        class="badge bg-transparent align-self-center ml-auto">Coming soon</span></a>
                        </li>
                    </ul>
                </li>

                <!-- /main -->

                <li class="nav-item-header">
                    <div class="text-uppercase font-size-xs line-height-xs">Manutenção</div>
                    <i class="icon-menu" title="Layout options"></i></li>

                <li class="nav-item nav-item-submenu">
                    <a href="#" class="nav-link"><i class="icon-stack2"></i> <span>Emulador</span></a>

                    <ul class="nav nav-group-sub" data-submenu-title="SWF">
                        <li class="nav-item"><a href="{$SITE_URL}reinstallEmu" class="nav-link">Reinstalar</a></li>
                        <li class="nav-item"><a href="{$SITE_URL}emuLogs" class="nav-link">Logs</a></li>

                    </ul>
                </li>

                <li class="nav-item nav-item-submenu">
                    <a href="#" class="nav-link"><i class="icon-earth"></i> <span>Website</span></a>

                    <ul class="nav nav-group-sub" data-submenu-title="SWF">
                        <li class="nav-item"><a href="{$SITE_URL}reinstallCms" class="nav-link">Reinstalar</a></li>
                        <li class="nav-item"><a href="{$SITE_URL}configCms" class="nav-link">Configurar</a></li>
                        <li class="nav-item"><a href="{$SITE_URL}maintenanceCms" class="nav-link">Manutenção</a></li>

                    </ul>
                </li>


                <li class="nav-item nav-item-submenu">
                    <a href="#" class="nav-link"><i class="icon-database"></i> <span>Banco de dados</span></a>

                    <ul class="nav nav-group-sub" data-submenu-title="SWF">
                        <li class="nav-item"><a href="{$SITE_URL}reinstallDb" class="nav-link">Reinstalar</a></li>
                        <li class="nav-item"><a href="{$SITE_URL}backupDb" class="nav-link">Backup</a></li>

                    </ul>
                </li>

                <!-- Layout -->
                <li class="nav-item-header">
                    <div class="text-uppercase font-size-xs line-height-xs">Configurações</div>
                    <i class="icon-menu" title="Layout options"></i></li>
                <li class="nav-item nav-item-submenu">
                    <a href="#" class="nav-link"><i class="icon-stack2"></i> <span>SWF</span></a>

                    <ul class="nav nav-group-sub" data-submenu-title="SWF">
                        <li class="nav-item"><a href="{$SITE_URL}editSwf/variables" class="nav-link">Editar
                                Variables</a>
                        <li class="nav-item"><a href="{$SITE_URL}editSwf/texts" class="nav-link">Editar Texts</a>
                        <li class="nav-item"><a href="layout_fixed_navbar.html" class="nav-link">Emblemas</a>
                        </li>
                    </ul>
                </li>

                <li class="nav-item nav-item-submenu">
                    <a href="#" class="nav-link"><i class="icon-cog"></i> <span>Hotel</span></a>

                    <ul class="nav nav-group-sub" data-submenu-title="SWF">
                        <li class="nav-item"><a href="layout_fixed_navbar.html" class="nav-link">Msg. de boas vindas</a>
                        </li>
                        <li class="nav-item"><a href="layout_fixed_navbar.html" class="nav-link">Outras
                                configurações</a>
                        </li>
                    </ul>
                </li>

                <!-- /layout -->

            </ul>
        </div>
        <!-- /main navigation -->

    </div>
    <!-- /sidebar content -->

</div>
<!-- /main sidebar -->

