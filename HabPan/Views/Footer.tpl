<!-- Footer -->
<div class="navbar navbar-expand-lg navbar-light">
    <div class="text-center d-lg-none w-100">
        <button type="button" class="navbar-toggler dropdown-toggle" data-toggle="collapse"
                data-target="#navbar-footer">
            <i class="icon-unfold mr-2"></i>
            Footer
        </button>
    </div>

    <div class="navbar-collapse collapse" id="navbar-footer">
					<span class="navbar-text">
						&copy; 2015 - 2018. <a href="#">HabPan</a> por <a
                                href="https://oblivion.host" target="_blank">Oblivion Team</a>
					</span>

        <ul class="navbar-nav ml-lg-auto">
            <li class="nav-item"><a href="https://kopyov.ticksy.com/" class="navbar-nav-link" target="_blank"><i
                            class="icon-lifebuoy mr-2"></i> Suporte</a></li>
            <li class="nav-item"><a href="http://demo.interface.club/limitless/docs/" class="navbar-nav-link"
                                    target="_blank"><i class="icon-file-text2 mr-2"></i> FAQs</a></li>
            <li class="nav-item"><a
                        href="https://themeforest.net/item/limitless-responsive-web-application-kit/13080328?ref=kopyov"
                        class="navbar-nav-link font-weight-semibold"><span class="text-pink-400"><i
                                class="icon-cart2 mr-2"></i> Comprar</span></a></li>
        </ul>
    </div>
</div>
<!-- /footer -->

</div>
<!-- /main content -->

</div>
<!-- /page content -->

<!-- Core JS files -->
<script src="{$SITE_URL}/assets/js/main/jquery.min.js"></script>
<script src="{$SITE_URL}/assets/js/main/bootstrap.bundle.min.js"></script>
<script src="{$SITE_URL}/assets/js/plugins/loaders/blockui.min.js"></script>
<!-- /core JS files -->

<!-- Theme JS files -->
<script src="{$SITE_URL}/assets/js/app.js"></script>


<script src=".{$SITE_URL}/assets/js/plugins/visualization/d3/d3.min.js"></script>
<script src=".{$SITE_URL}/assets/js/plugins/visualization/d3/d3_tooltip.js"></script>
<script src=".{$SITE_URL}/assets/js/plugins/forms/styling/switchery.min.js"></script>
<script src=".{$SITE_URL}/assets/js/plugins/forms/selects/bootstrap_multiselect.js"></script>
<script src=".{$SITE_URL}/assets/js/plugins/ui/moment/moment.min.js"></script>
<script src=".{$SITE_URL}/assets/js/plugins/pickers/daterangepicker.js"></script>

<script src="{$SITE_URL}/assets/js/demo_pages/dashboard.js"></script>
<!-- /theme JS files -->
