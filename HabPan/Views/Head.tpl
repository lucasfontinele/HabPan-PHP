<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>{$SITE_NAME} - {$PAGE_TITLE}</title>

    <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
    <link href="{$SITE_URL}/assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
    <link href="{$SITE_URL}/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="{$SITE_URL}/assets/css/bootstrap_limitless.min.css" rel="stylesheet" type="text/css">
    <link href="{$SITE_URL}/assets/css/layout.min.css?1" rel="stylesheet" type="text/css">
    <link href="{$SITE_URL}/assets/css/components.min.css" rel="stylesheet" type="text/css">
    <link href="{$SITE_URL}/assets/css/colors.min.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <!-- /global stylesheets -->
