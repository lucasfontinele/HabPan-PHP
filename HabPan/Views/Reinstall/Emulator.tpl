{include file="Head.tpl"}
</head>
<body>
{include file="TopBar.tpl"}
<!-- Page content -->
<div class="page-content">
    {include file="SideBar.tpl"}

    <!-- Main content -->
    <div class="content-wrapper">

        <!-- Content area -->
        <div class="content">

            <div class="card">
                <div class="card-header header-elements-inline">
                    <h5 class="card-title">{$PAGE_TITLE}</h5>

                </div>

                <div class="card-body">

                    <form method="post" class="form-horizontal" id="sendForm" action="javascript:reinstall()">
                        <div id="formResult"></div>
                        <div class="form-group">
                            <label>Selecione um emulador abaixo e em seguida pressione reinstalar</label>
                            <select class="form-control select" data-container-css-class="bg-teal-400" name="emu"
                                    data-fouc>

                                {foreach $emulators as $row}
                                    <option value="{$row.id}">{$row.title} {($row.vip_only == '1') ? '(VIP)' : null}</option>
                                {/foreach}
                            </select>
                            <input type="hidden" value="{$CSRF_TOKEN}" name="token">
                        </div>
                        <button name="submit" type="submit" class="btn btn-info float-right">Reinstalar <i
                                    class="icon-arrow-right14 position-right"></i></button>
                    </form>
                </div>
            </div>
        </div>
        <!-- /content area -->
        {include file="Footer.tpl"}

        <script src="{$SITE_URL}assets/js/plugins/forms/selects/select2.min.js"></script>

        <script>
            $('.select').select2({
                minimumResultsForSearch: Infinity
            });

            function reinstall() {
                const result = $("#formResult");
                $.post('{$SITE_URL}api/reinstallEmu', $("#sendForm").serialize(), function (data) {
                    if (data['message'] === 'success') {
                        result.html("<div class=\"alert alert-success alert-dismissible fade show\" role=\"alert\">" +
                            "  <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">" +
                            "    <span aria-hidden=\"true\">&times;</span>" +
                            "  </button>Seu emulador está sendo reinstalado!</div>");
                    } else {
                        result.html("<div class=\"alert alert-danger alert-dismissible fade show\" role=\"alert\">" +
                            "  <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">" +
                            "    <span aria-hidden=\"true\">&times;</span>" +
                            "  </button>" +
                            data['message'] +
                            "</div>");
                    }
                }, 'json');
            }
        </script>
</body>
</html>