{include file="Head.tpl"}
</head>
<body>
{include file="TopBar.tpl"}

<!-- Page content -->
<div class="page-content">

    {include file="SideBar.tpl"}

    <!-- Main content -->
    <div class="content-wrapper">

        <br>

        <!-- Content area -->
        <div class="content pt-0">


            <!-- Dashboard content -->
            <div class="row">
                <div class="col-xl-8">

                    <!-- Quick stats boxes -->
                    <div class="row">
                        <div class="col-lg-4">

                            <!-- Members online -->
                            <div class="card bg-teal-400">
                                <div class="card-body">
                                    <div class="d-flex">
                                        <h3 class="font-weight-semibold mb-0">3,450</h3>
                                        <span class="badge bg-teal-800 badge-pill align-self-center ml-auto">+53,6%</span>
                                    </div>

                                    <div>
                                        Members online
                                        <div class="font-size-sm opacity-75">489 avg</div>
                                    </div>
                                </div>

                                <div class="container-fluid">
                                    <div id="members-online"></div>
                                </div>
                            </div>
                            <!-- /members online -->

                        </div>

                        <div class="col-lg-4">

                            <!-- Current server load -->
                            <div class="card bg-pink-400">
                                <div class="card-body">
                                    <div class="d-flex">
                                        <h3 class="font-weight-semibold mb-0">49.4%</h3>
                                        <div class="list-icons ml-auto">
                                            <div class="list-icons-item dropdown">
                                                <a href="#" class="list-icons-item dropdown-toggle"
                                                   data-toggle="dropdown"><i class="icon-cog3"></i></a>
                                                <div class="dropdown-menu dropdown-menu-right">
                                                    <a href="#" class="dropdown-item"><i class="icon-sync"></i> Update
                                                        data</a>
                                                    <a href="#" class="dropdown-item"><i
                                                                class="icon-list-unordered"></i> Detailed log</a>
                                                    <a href="#" class="dropdown-item"><i class="icon-pie5"></i>
                                                        Statistics</a>
                                                    <a href="#" class="dropdown-item"><i class="icon-cross3"></i> Clear
                                                        list</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div>
                                        Current server load
                                        <div class="font-size-sm opacity-75">34.6% avg</div>
                                    </div>
                                </div>

                                <div id="server-load"></div>
                            </div>
                            <!-- /current server load -->

                        </div>

                        <div class="col-lg-4">

                            <!-- Today's revenue -->
                            <div class="card bg-blue-400">
                                <div class="card-body">
                                    <div class="d-flex">
                                        <h3 class="font-weight-semibold mb-0">$18,390</h3>
                                        <div class="list-icons ml-auto">
                                            <a class="list-icons-item" data-action="reload"></a>
                                        </div>
                                    </div>

                                    <div>
                                        Today's revenue
                                        <div class="font-size-sm opacity-75">$37,578 avg</div>
                                    </div>
                                </div>

                                <div id="today-revenue"></div>
                            </div>
                            <!-- /today's revenue -->

                        </div>
                    </div>
                    <!-- /quick stats boxes -->

                    <!-- Latest posts -->
                    <div class="card">
                        <div class="card-header header-elements-inline">
                            <h6 class="card-title">Últimas postagens</h6>
                            <div class="header-elements">
                                <div class="list-icons">
                                    <a class="list-icons-item" data-action="collapse"></a>
                                    <a class="list-icons-item" data-action="reload"></a>
                                    <a class="list-icons-item" data-action="remove"></a>
                                </div>
                            </div>
                        </div>

                        <div class="card-body pb-0">
                            <div class="row">
                                <div class="col-xl-6">
                                    <div class="media flex-column flex-sm-row mt-0 mb-3">
                                        <div class="mr-sm-3 mb-2 mb-sm-0">
                                            <div class="card-img-actions">
                                                <a href="#">
                                                    <img src="{$SITE_URL}/assets/images/placeholders/placeholder.jpg"
                                                         class="img-fluid img-preview rounded" alt="">
                                                    <span class="card-img-actions-overlay card-img"><i
                                                                class="icon-play3 icon-2x"></i></span>
                                                </a>
                                            </div>
                                        </div>

                                        <div class="media-body">
                                            <h6 class="media-title"><a href="#">Up unpacked friendly</a></h6>
                                            <ul class="list-inline list-inline-dotted text-muted mb-2">
                                                <li class="list-inline-item"><i class="icon-book-play mr-2"></i> Video
                                                    tutorials
                                                </li>
                                            </ul>
                                            The him father parish looked has sooner. Attachment frequently terminated
                                            son hello...
                                        </div>
                                    </div>

                                    <div class="media flex-column flex-sm-row mt-0 mb-3">
                                        <div class="mr-sm-3 mb-2 mb-sm-0">
                                            <div class="card-img-actions">
                                                <a href="#">
                                                    <img src="{$SITE_URL}/assets/images/placeholders/placeholder.jpg"
                                                         class="img-fluid img-preview rounded" alt="">
                                                    <span class="card-img-actions-overlay card-img"><i
                                                                class="icon-play3 icon-2x"></i></span>
                                                </a>
                                            </div>
                                        </div>

                                        <div class="media-body">
                                            <h6 class="media-title"><a href="#">It allowance prevailed</a></h6>
                                            <ul class="list-inline list-inline-dotted text-muted mb-2">
                                                <li class="list-inline-item"><i class="icon-book-play mr-2"></i> Video
                                                    tutorials
                                                </li>
                                            </ul>
                                            Alteration literature to or an sympathize mr imprudence. Of is ferrars
                                            subject enjoyed...
                                        </div>
                                    </div>
                                </div>

                                <div class="col-xl-6">
                                    <div class="media flex-column flex-sm-row mt-0 mb-3">
                                        <div class="mr-sm-3 mb-2 mb-sm-0">
                                            <div class="card-img-actions">
                                                <a href="#">
                                                    <img src="{$SITE_URL}/assets/images/placeholders/placeholder.jpg"
                                                         class="img-fluid img-preview rounded" alt="">
                                                    <span class="card-img-actions-overlay card-img"><i
                                                                class="icon-play3 icon-2x"></i></span>
                                                </a>
                                            </div>
                                        </div>

                                        <div class="media-body">
                                            <h6 class="media-title"><a href="#">Case read they must</a></h6>
                                            <ul class="list-inline list-inline-dotted text-muted mb-2">
                                                <li class="list-inline-item"><i class="icon-book-play mr-2"></i> Video
                                                    tutorials
                                                </li>
                                            </ul>
                                            On it differed repeated wandered required in. Then girl neat why yet knew
                                            rose spot...
                                        </div>
                                    </div>

                                    <div class="media flex-column flex-sm-row mt-0 mb-3">
                                        <div class="mr-sm-3 mb-2 mb-sm-0">
                                            <div class="card-img-actions">
                                                <a href="#">
                                                    <img src="{$SITE_URL}/assets/images/placeholders/placeholder.jpg"
                                                         class="img-fluid img-preview rounded" alt="">
                                                    <span class="card-img-actions-overlay card-img"><i
                                                                class="icon-play3 icon-2x"></i></span>
                                                </a>
                                            </div>
                                        </div>

                                        <div class="media-body">
                                            <h6 class="media-title"><a href="#">Too carriage attended</a></h6>
                                            <ul class="list-inline list-inline-dotted text-muted mb-2">
                                                <li class="list-inline-item"><i class="icon-book-play mr-2"></i> FAQ
                                                    section
                                                </li>
                                            </ul>
                                            Marianne or husbands if at stronger ye. Considered is as middletons
                                            uncommonly...
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /latest posts -->

                </div>

                <div class="col-xl-4">

                    <!-- Main charts -->
                    <div class="row">
                        <div class="col-sm-12">

                            <!-- Traffic sources -->
                            <div class="card">
                                <div class="card-header header-elements-inline">
                                    <div class="header-elements">
                                        <div class="form-check form-check-right form-check-switchery form-check-switchery-sm">
                                            <label class="form-check-label">
                                                Ações rápidas:
                                            </label>
                                        </div>
                                    </div>
                                </div>

                                <div class="card-body py-0">
                                    <div class="row" >
                                        <div class="col-md-2"></div>
                                        <div class="col-sm-2">
                                            <a href="#"
                                               class="btn bg-transparent border-success text-success rounded-round border-2 btn-icon ">
                                                <i class="fas fa-play" style="padding: 3px"></i>
                                            </a>
                                        </div>

                                        <div class="col-sm-2">
                                            <a href="#"
                                               class="btn bg-transparent border-danger-800 text-danger-800 rounded-round border-2 btn-icon mr-3">
                                                <i class="fas fa-pause" style="padding: 3px"></i> </a>
                                        </div>

                                        <div class="col-sm-2">
                                            <a href="#"
                                               class="btn bg-transparent border-primary-300 text-primary-300 rounded-round border-2 btn-icon mr-3">
                                                <i class="fas fa-spin fa-sync-alt" style="padding: 2px"></i> </a>

                                            <div class="w-75 mx-auto mb-3" id="new-visitors"></div>

                                        </div>
                                        <div class="col-sm-1">
                                            <a href="#"
                                               class="btn bg-transparent border-warning-300 text-warning-300 rounded-round border-2 btn-icon mr-3">
                                                <i class="fas fa-exclamation-triangle" style="padding: 1px"></i> </a>


                                        </div>


                                    </div>
                                </div>

                                <div class="chart position-relative" id="traffic-sources"></div>
                            </div>
                            <!-- /traffic sources -->

                        </div>

                    </div>
                    <!-- /main charts -->

                </div>


            </div>
            <!-- /dashboard content -->

        </div>
        <!-- /content area -->


    </div>
    <!-- /main content -->

</div>
<!-- /page content -->


{include file="Footer.tpl"}


</body>
</html>
