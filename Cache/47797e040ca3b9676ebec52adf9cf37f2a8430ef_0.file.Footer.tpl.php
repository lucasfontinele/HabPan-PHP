<?php
/* Smarty version 3.1.33, created on 2019-02-13 02:00:34
  from '/mnt/c/xampp/habpan-php/HabPan/Views/Footer.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5c637a426ca150_35378345',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '47797e040ca3b9676ebec52adf9cf37f2a8430ef' => 
    array (
      0 => '/mnt/c/xampp/habpan-php/HabPan/Views/Footer.tpl',
      1 => 1549131048,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5c637a426ca150_35378345 (Smarty_Internal_Template $_smarty_tpl) {
?><!-- Footer -->
<div class="navbar navbar-expand-lg navbar-light">
    <div class="text-center d-lg-none w-100">
        <button type="button" class="navbar-toggler dropdown-toggle" data-toggle="collapse"
                data-target="#navbar-footer">
            <i class="icon-unfold mr-2"></i>
            Footer
        </button>
    </div>

    <div class="navbar-collapse collapse" id="navbar-footer">
					<span class="navbar-text">
						&copy; 2015 - 2018. <a href="#">HabPan</a> por <a
                                href="https://oblivion.host" target="_blank">Oblivion Team</a>
					</span>

        <ul class="navbar-nav ml-lg-auto">
            <li class="nav-item"><a href="https://kopyov.ticksy.com/" class="navbar-nav-link" target="_blank"><i
                            class="icon-lifebuoy mr-2"></i> Suporte</a></li>
            <li class="nav-item"><a href="http://demo.interface.club/limitless/docs/" class="navbar-nav-link"
                                    target="_blank"><i class="icon-file-text2 mr-2"></i> FAQs</a></li>
            <li class="nav-item"><a
                        href="https://themeforest.net/item/limitless-responsive-web-application-kit/13080328?ref=kopyov"
                        class="navbar-nav-link font-weight-semibold"><span class="text-pink-400"><i
                                class="icon-cart2 mr-2"></i> Comprar</span></a></li>
        </ul>
    </div>
</div>
<!-- /footer -->

</div>
<!-- /main content -->

</div>
<!-- /page content -->

<!-- Core JS files -->
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
/assets/js/main/jquery.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
/assets/js/main/bootstrap.bundle.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
/assets/js/plugins/loaders/blockui.min.js"><?php echo '</script'; ?>
>
<!-- /core JS files -->

<!-- Theme JS files -->
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
/assets/js/app.js"><?php echo '</script'; ?>
>


<?php echo '<script'; ?>
 src=".<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
/assets/js/plugins/visualization/d3/d3.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src=".<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
/assets/js/plugins/visualization/d3/d3_tooltip.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src=".<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
/assets/js/plugins/forms/styling/switchery.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src=".<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
/assets/js/plugins/forms/selects/bootstrap_multiselect.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src=".<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
/assets/js/plugins/ui/moment/moment.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src=".<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
/assets/js/plugins/pickers/daterangepicker.js"><?php echo '</script'; ?>
>

<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
/assets/js/demo_pages/dashboard.js"><?php echo '</script'; ?>
>
<!-- /theme JS files -->
<?php }
}
