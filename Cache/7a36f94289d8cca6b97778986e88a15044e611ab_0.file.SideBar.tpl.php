<?php
/* Smarty version 3.1.33, created on 2019-02-13 03:05:07
  from '/mnt/c/xampp/habpan-php/HabPan/Views/SideBar.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5c638963b01301_36082996',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '7a36f94289d8cca6b97778986e88a15044e611ab' => 
    array (
      0 => '/mnt/c/xampp/habpan-php/HabPan/Views/SideBar.tpl',
      1 => 1550027105,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5c638963b01301_36082996 (Smarty_Internal_Template $_smarty_tpl) {
?><!-- Main sidebar -->
<div class="sidebar sidebar-dark sidebar-main sidebar-expand-md">

    <!-- Sidebar mobile toggler -->
    <div class="sidebar-mobile-toggler text-center">
        <a href="#" class="sidebar-mobile-main-toggle">
            <i class="icon-arrow-left8"></i>
        </a>
        Navigation
        <a href="#" class="sidebar-mobile-expand">
            <i class="icon-screen-full"></i>
            <i class="icon-screen-normal"></i>
        </a>
    </div>
    <!-- /sidebar mobile toggler -->


    <!-- Sidebar content -->
    <div class="sidebar-content">

        <!-- User menu -->
        <div class="sidebar-user">
            <div class="card-body">
                <div class="media">
                    <div class="mr-3">
                        <a href="#"><img src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
/assets/images/placeholders/placeholder.jpg"
                                         width="38" height="38" class="rounded-circle" alt=""></a>
                    </div>

                    <div class="media-body">
                        <div class="media-title font-weight-semibold"><?php echo $_smarty_tpl->tpl_vars['session']->value->getUser()->getName();?>
</div>
                        <div class="font-size-xs opacity-50">
                            <i class="icon-pin font-size-sm"></i> &nbsp;Santa Ana, CA
                        </div>
                    </div>

                    <div class="ml-3 align-self-center">
                        <a href="#" class="text-white"><i class="icon-cog3"></i></a>
                    </div>
                </div>
            </div>
        </div>
        <!-- /user menu -->


        <!-- Main navigation -->
        <div class="card card-sidebar-mobile">
            <ul class="nav nav-sidebar" data-nav-type="accordion">

                <!-- Main -->
                <li class="nav-item-header">
                    <div class="text-uppercase font-size-xs line-height-xs">Principal</div>
                    <i class="icon-menu" title="Main"></i></li>
                <li class="nav-item">
                    <a href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
" class="nav-link active">
                        <i class="icon-home4"></i>
                        <span>
									Dashboard
								</span>
                    </a>
                </li>

                <li class="nav-item nav-item-submenu">
                    <a href="#" class="nav-link"><i class="icon-user"></i> <span>Sua conta</span></a>

                    <ul class="nav nav-group-sub" data-submenu-title="Layouts">
                        <li class="nav-item"><a href="../../../../layout_1/LTR/default/full/<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
"
                                                class="nav-link">Default layout</a></li>
                        <li class="nav-item"><a href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
" class="nav-link active">Layout 2</a></li>
                        <li class="nav-item"><a href="../../../../layout_3/LTR/default/full/<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
"
                                                class="nav-link">Layout 3</a></li>
                        <li class="nav-item"><a href="../../../../layout_4/LTR/default/full/<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
"
                                                class="nav-link">Layout 4</a></li>
                        <li class="nav-item"><a href="../../../../layout_5/LTR/default/full/<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
"
                                                class="nav-link">Layout 5</a></li>
                        <li class="nav-item"><a href="../../../../layout_6/LTR/default/full/<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
"
                                                class="nav-link disabled">Layout 6 <span
                                        class="badge bg-transparent align-self-center ml-auto">Coming soon</span></a>
                        </li>
                    </ul>
                </li>

                <!-- /main -->

                <li class="nav-item-header">
                    <div class="text-uppercase font-size-xs line-height-xs">Manutenção</div>
                    <i class="icon-menu" title="Layout options"></i></li>

                <li class="nav-item nav-item-submenu">
                    <a href="#" class="nav-link"><i class="icon-stack2"></i> <span>Emulador</span></a>

                    <ul class="nav nav-group-sub" data-submenu-title="SWF">
                        <li class="nav-item"><a href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
reinstallEmu" class="nav-link">Reinstalar</a></li>
                        <li class="nav-item"><a href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
emuLogs" class="nav-link">Logs</a></li>

                    </ul>
                </li>

                <li class="nav-item nav-item-submenu">
                    <a href="#" class="nav-link"><i class="icon-earth"></i> <span>Website</span></a>

                    <ul class="nav nav-group-sub" data-submenu-title="SWF">
                        <li class="nav-item"><a href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
reinstallCms" class="nav-link">Reinstalar</a></li>
                        <li class="nav-item"><a href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
configCms" class="nav-link">Configurar</a></li>
                        <li class="nav-item"><a href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
maintenanceCms" class="nav-link">Manutenção</a></li>

                    </ul>
                </li>


                <li class="nav-item nav-item-submenu">
                    <a href="#" class="nav-link"><i class="icon-database"></i> <span>Banco de dados</span></a>

                    <ul class="nav nav-group-sub" data-submenu-title="SWF">
                        <li class="nav-item"><a href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
reinstallDb" class="nav-link">Reinstalar</a></li>
                        <li class="nav-item"><a href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
backupDb" class="nav-link">Backup</a></li>

                    </ul>
                </li>

                <!-- Layout -->
                <li class="nav-item-header">
                    <div class="text-uppercase font-size-xs line-height-xs">Configurações</div>
                    <i class="icon-menu" title="Layout options"></i></li>
                <li class="nav-item nav-item-submenu">
                    <a href="#" class="nav-link"><i class="icon-stack2"></i> <span>SWF</span></a>

                    <ul class="nav nav-group-sub" data-submenu-title="SWF">
                        <li class="nav-item"><a href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
editSwf/variables" class="nav-link">Editar
                                Variables</a>
                        <li class="nav-item"><a href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
editSwf/texts" class="nav-link">Editar Texts</a>
                        <li class="nav-item"><a href="layout_fixed_navbar.html" class="nav-link">Emblemas</a>
                        </li>
                    </ul>
                </li>

                <li class="nav-item nav-item-submenu">
                    <a href="#" class="nav-link"><i class="icon-cog"></i> <span>Hotel</span></a>

                    <ul class="nav nav-group-sub" data-submenu-title="SWF">
                        <li class="nav-item"><a href="layout_fixed_navbar.html" class="nav-link">Msg. de boas vindas</a>
                        </li>
                        <li class="nav-item"><a href="layout_fixed_navbar.html" class="nav-link">Outras
                                configurações</a>
                        </li>
                    </ul>
                </li>

                <!-- /layout -->

            </ul>
        </div>
        <!-- /main navigation -->

    </div>
    <!-- /sidebar content -->

</div>
<!-- /main sidebar -->

<?php }
}
