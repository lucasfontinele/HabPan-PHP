<?php

use HabPan\Core;

session_start();

require_once './Settings.php';
require CWD . '/vendor/autoload.php';

$core = new Core();
$core->getRoute()->renderPage();
$core->getCron()->runCron(SITE_URL);

