/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

CREATE TABLE IF NOT EXISTS `crons` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `prio` int(11) NOT NULL DEFAULT '5',
  `enabled` enum('0','1') NOT NULL DEFAULT '1',
  `scriptfile` varchar(50) NOT NULL,
  `last_exec` int(11) NOT NULL,
  `exec_every` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `enabled` (`enabled`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*!40000 ALTER TABLE `crons` DISABLE KEYS */;
/*!40000 ALTER TABLE `crons` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `emulators` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `vip_only` enum('0','1') NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

/*!40000 ALTER TABLE `emulators` DISABLE KEYS */;
INSERT INTO `emulators` (`id`, `title`, `vip_only`) VALUES
	(1, 'Comet', '0'),
	(2, 'Oblivion v1', '0'),
	(3, 'MarshMallow', '0'),
	(4, 'Oblivion v2', '1');
/*!40000 ALTER TABLE `emulators` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `hotels` (
  `user_id` int(11) NOT NULL,
  `tcp_port` int(11) NOT NULL,
  `mus_port` int(11) NOT NULL,
  `credits_loop` int(11) NOT NULL,
  `diamonds_loop` int(11) NOT NULL,
  `pixels_loop` int(11) NOT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `tcp_port` (`tcp_port`),
  UNIQUE KEY `mus_port` (`mus_port`),
  CONSTRAINT `FK_UserId_Users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*!40000 ALTER TABLE `hotels` DISABLE KEYS */;
INSERT INTO `hotels` (`user_id`, `tcp_port`, `mus_port`, `credits_loop`, `diamonds_loop`, `pixels_loop`) VALUES
	(1, 0, 0, 0, 0, 0);
/*!40000 ALTER TABLE `hotels` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `ranks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `IsAdmin` enum('0','1') NOT NULL DEFAULT '0',
  `IsMod` enum('0','1') NOT NULL DEFAULT '0',
  `IsReseller` enum('0','1') NOT NULL DEFAULT '0',
  `IsPremium` enum('0','1') NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

/*!40000 ALTER TABLE `ranks` DISABLE KEYS */;
INSERT INTO `ranks` (`id`, `IsAdmin`, `IsMod`, `IsReseller`, `IsPremium`) VALUES
	(1, '0', '0', '0', '0');
/*!40000 ALTER TABLE `ranks` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `resellers_data` (
  `user_id` int(11) NOT NULL,
  `hotel_limit` int(11) NOT NULL,
  `subres_limit` int(11) NOT NULL,
  PRIMARY KEY (`user_id`),
  CONSTRAINT `FK_resellers_data_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*!40000 ALTER TABLE `resellers_data` DISABLE KEYS */;
/*!40000 ALTER TABLE `resellers_data` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `servers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hostname` varchar(255) NOT NULL,
  `port` int(11) NOT NULL,
  `limit` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

/*!40000 ALTER TABLE `servers` DISABLE KEYS */;
INSERT INTO `servers` (`id`, `hostname`, `port`, `limit`) VALUES
	(1, '127.0.0.1', 1000, 10);
/*!40000 ALTER TABLE `servers` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `servers_auth` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `auth_key` varchar(255) NOT NULL,
  `expiration` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_servers_auth_users` (`user_id`),
  CONSTRAINT `FK_servers_auth_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*!40000 ALTER TABLE `servers_auth` DISABLE KEYS */;
/*!40000 ALTER TABLE `servers_auth` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `servers_settings` (
  `set_key` varchar(50) NOT NULL,
  `set_val` varchar(255) NOT NULL,
  PRIMARY KEY (`set_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*!40000 ALTER TABLE `servers_settings` DISABLE KEYS */;
INSERT INTO `servers_settings` (`set_key`, `set_val`) VALUES
	('MAIN_URL', 'http://localhost'),
	('WHM_TOKEN', '7NOZ95JPZBGAQTBBD2KCLP8G0EP26D21');
/*!40000 ALTER TABLE `servers_settings` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(75) NOT NULL,
  `real_name` varchar(75) NOT NULL,
  `mail` varchar(125) NOT NULL,
  `password` varchar(255) NOT NULL,
  `rank` int(11) NOT NULL,
  `status` enum('enabled','disabled') NOT NULL DEFAULT 'enabled',
  PRIMARY KEY (`id`),
  KEY `FK_users_ranks` (`rank`),
  CONSTRAINT `FK_users_ranks` FOREIGN KEY (`rank`) REFERENCES `ranks` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `username`, `real_name`, `mail`, `password`, `rank`, `status`) VALUES
	(1, 'admin', '', '123@321.com', '$2y$10$/X806b4o0uDYKFDfTlDsieXrnUxB9hqpHWcxVhbgHZYqQF6zgl4lC', 1, 'enabled');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
